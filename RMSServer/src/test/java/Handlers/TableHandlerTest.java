/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author basvg
 */
public class TableHandlerTest {
    
    public TableHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getAllTables method, of class TableHandler.
     */
    @Test
    public void testGetAllTables() throws SQLException {
        System.out.println("getAllTables");
        TableHandler instance = new TableHandler();
        instance.makeConnection();
        assertThat(instance.getAllTables(), matchesJsonSchemaInClasspath("All_Tables.json"));
        instance.Close();
    }
    
}
