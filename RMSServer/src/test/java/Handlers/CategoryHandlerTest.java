/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author basvg
 */
public class CategoryHandlerTest {
    
    public CategoryHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getcategorieWithID method, of class CategoryHandler.
     */
    @Test
    public void testGetcategorieWithID() throws SQLException {
        System.out.println("getcategorieWithID");
        CategoryHandler instance = new CategoryHandler();
        instance.makeConnection();
        assertThat(instance.getcategorieWithID(14), matchesJsonSchemaInClasspath("Category.json"));
    }

    /**
     * Test of getAllCategories method, of class CategoryHandler.
     */
    @Test
    public void testGetAllCategories() throws SQLException {
        System.out.println("getAllCategories");
        CategoryHandler instance = new CategoryHandler();
        instance.makeConnection();
        assertThat(instance.getAllCategories(), matchesJsonSchemaInClasspath("All_Categories.json"));
        instance.Close();
    }
    
}
