/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import static DataLayer.rmsdata.Tables.CATEGORIES;
import static DataLayer.rmsdata.Tables.MENUITEMS;
import static DataLayer.rmsdata.Tables.MENUITEMS_HAS_PRODUCTS;
import DataLayer.rmsdata.tables.records.CategoriesRecord;
import DataLayer.rmsdata.tables.records.MenuitemsHasProductsRecord;
import DataLayer.rmsdata.tables.records.MenuitemsRecord;
import static Tools.JSONTools.JSONparser;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.sql.SQLException;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class MenuItemHandlerTest {

    public MenuItemHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addMenuItem method, of class MenuItemHandler.
     */
    @Test
    public void testAddMenuItem() throws Exception {
        System.out.println("addMenuItem");
        MenuItemHandler instance = new MenuItemHandler();

        int productID1 = 116;
        int productID2 = 117;

        // Create new menuItem
        JSONArray IDsArray = new JSONArray();
        IDsArray.add(productID1);
        IDsArray.add(productID2);

        JSONObject menuItem = new JSONObject();
        menuItem.put("id", -1); // -1 to add new menuItem
        menuItem.put("name", "MenuItemUnitTesting");
        menuItem.put("category", "Test Category");
        menuItem.put("description", "This is a menuItem to test unit tests");
        menuItem.put("price", 1.20);
        menuItem.put("IDs", IDsArray);

        instance.addMenuItem(menuItem.toString());

        // Get the new menuItem
        JSONObject allMenuItems = JSONparser(instance.getAllMenuItems());
        JSONArray items = (JSONArray) allMenuItems.get("items");
        JSONObject lastMenuItem = (JSONObject) items.get(items.size() - 1);
        int id = (int) (long) lastMenuItem.get("id");

        JSONObject newMenuItem = JSONparser(instance.getMenuItem(id));
        
        assertEquals(menuItem.get("name"), newMenuItem.get("name"));

        // Update the menuItem
        menuItem.put("id", id);
        menuItem.put("description", "Updated");
        
        instance.addMenuItem(menuItem.toString());
        
        // Get updated menuItem
        newMenuItem = JSONparser(instance.getMenuItem(id));
        
        assertEquals("Updated", newMenuItem.get("desc").toString());
        
        // Delete menuItem (i.e) set desc to NA       
        JSONArray arrayNA = new JSONArray();
        arrayNA.add(id);
        
        JSONObject menuItemNA = new JSONObject();
        menuItemNA.put("IDs", arrayNA);
        
        instance.deleteItems(menuItemNA.toString());
        
        // Get updated menuItem with desc NA
        newMenuItem = JSONparser(instance.getMenuItem(id));
        
        assertEquals("NA", newMenuItem.get("desc").toString());
        
        // Revert menuItem
        MenuitemsHasProductsRecord deleteProd1 = instance.DBContext.selectFrom(MENUITEMS_HAS_PRODUCTS).where(MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.equal(productID1)).and(MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(id)).fetchOne();
        MenuitemsHasProductsRecord deleteProd2 = instance.DBContext.selectFrom(MENUITEMS_HAS_PRODUCTS).where(MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.equal(productID2)).and(MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(id)).fetchOne();
        MenuitemsRecord deleteMenuItem = instance.DBContext.selectFrom(MENUITEMS).where(MENUITEMS.ID.equal(id)).fetchOne();
        CategoriesRecord deleteCat = instance.DBContext.selectFrom(CATEGORIES).where(CATEGORIES.NAME.equal("Test Category")).fetchOne();
        deleteProd1.delete();
        deleteProd2.delete();
        deleteMenuItem.delete();
        deleteCat.delete();
    }
//
//    /**
//     * Test of deleteItems method, of class MenuItemHandler.
//     */
//    @Test
//    public void testDeleteItems() throws Exception {
//        System.out.println("deleteItems");
//        String JSONData = "";
//        MenuItemHandler instance = new MenuItemHandler();
//        instance.deleteItems(JSONData);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getAllMenuItems method, of class MenuItemHandler.
     */
    @Test
    public void testGetAllMenuItems() throws SQLException {
        System.out.println("getAllMenuItems");
        MenuItemHandler instance = new MenuItemHandler();
        instance.makeConnection();
        assertThat(instance.getAllMenuItems(), matchesJsonSchemaInClasspath("All_MenuItems.json"));
    }

    /**
     * Test of getMenuItem method, of class MenuItemHandler.
     */
    @Test
    public void testGetMenuItem() throws SQLException {
        System.out.println("getMenuItem");
        int menuItemID = 34;
        MenuItemHandler instance = new MenuItemHandler();
        instance.makeConnection();
        assertThat(instance.getMenuItem(menuItemID), matchesJsonSchemaInClasspath("MenuItem.json"));
    }

//    /**
//     * Test of EditMenuItem method, of class MenuItemHandler.
//     */
//    @Test
//    public void testEditMenuItem() throws Exception {
//        System.out.println("EditMenuItem");
//        String JSONInput = "";
//        MenuItemHandler instance = new MenuItemHandler();
//        instance.EditMenuItem(JSONInput);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
