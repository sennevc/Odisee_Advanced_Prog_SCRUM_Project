/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.Tables;
import static DataLayer.rmsdata.Tables.STOCK;
import DataLayer.rmsdata.tables.Menuitems;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.records.StockRecord;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class StockHandlerTest {

    public StockHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updateStockProduct method, of class StockHandler.
     */
    @Test
    public void testUpdateStockProduct() throws Exception {
        System.out.println("updateStockProduct");
        int productID = 84;
        int newAmount = 55;
        StockHandler instance = new StockHandler();
        instance.makeConnection();

        // Get the old stock
        StockRecord oldStock = instance.DBContext.selectFrom(STOCK).where(STOCK.PRODUCT_ID.eq(productID)).fetchOne();

        // Create stock to be updated
        JSONObject stock = new JSONObject();
        stock.put("productID", productID);
        stock.put("amount", newAmount);

        // Update the product
        instance.updateStockProduct(stock.toString());

        // Get the new stock
        StockRecord newStock = instance.DBContext.selectFrom(STOCK).where(STOCK.PRODUCT_ID.eq(productID)).fetchOne();

        // Check if new stock matches 
        assertEquals((int) newStock.getQuantity(), newAmount);

        // Revert to old stock
        stock.put("amount", oldStock.getQuantity());
        instance.updateStockProduct(stock.toString());

        instance.Close();
    }

    /**
     * Test of replenishStockUponCancelling method, of class StockHandler.
     */
    @Test
    public void testReplenishStockUponCancelling() throws SQLException {
        System.out.println("replenishStockUponCancelling");

        int menuItemID = 34;
        StockHandler instance = new StockHandler();

        // Get the stock for the menuItems
        LinkedHashMap<Integer, Integer> oldStock = getProductsStock(menuItemID, instance);

        // replenishStock
        instance.replenishStockUponCancelling(menuItemID);

        // Get the new stock for the menuItems
        LinkedHashMap<Integer, Integer> newStock = getProductsStock(menuItemID, instance);

        // Check if they're plus one
        assertEquals((int) oldStock.values().iterator().next() + 1, (int) newStock.values().iterator().next());

        // Revert stock
        for (Map.Entry<Integer, Integer> entry : oldStock.entrySet()) {
            StockRecord prodStock = instance.DBContext.selectFrom(Tables.STOCK).where(Tables.STOCK.PRODUCT_ID.equal(entry.getKey())).fetchOne();
            prodStock.setQuantity(entry.getValue());
            prodStock.update();
        }
    }

    /**
     * Gets all the stock from products from one menuItem
     *
     * @param menuItemID
     * @return
     * @throws SQLException
     */
    private LinkedHashMap getProductsStock(int menuItemID, StockHandler handler) throws SQLException {
        LinkedHashMap<Integer, Integer> productsStock = new LinkedHashMap<Integer, Integer>();

        Result<?> menuItemProducts = handler.DBContext.select(Menuitems.MENUITEMS.ID.as("menuItemID"), MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.as("productID"))
                .from(Menuitems.MENUITEMS.join(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS)
                        .on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(Menuitems.MENUITEMS.ID)))
                .where(Menuitems.MENUITEMS.ID.eq(menuItemID))
                .fetch();

        for (Record product : menuItemProducts) {
            StockRecord stock = handler.DBContext.selectFrom(Tables.STOCK).where(Tables.STOCK.PRODUCT_ID.equal(Integer.parseInt(product.get("productID").toString()))).fetchOne();
            productsStock.put(stock.getProductId(), stock.getQuantity());
        }

        return productsStock;
    }
}
