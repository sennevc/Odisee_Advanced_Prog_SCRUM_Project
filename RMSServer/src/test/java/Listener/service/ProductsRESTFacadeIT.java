/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.builder.RequestSpecBuilder;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import static org.hamcrest.Matchers.*;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import com.jayway.restassured.response.Response;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author TOM
 */
public class ProductsRESTFacadeIT {

    private RestValidator validator;

    public ProductsRESTFacadeIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {   
        validator = new RestValidator("products");
    }

    @After
    public void tearDown() {
    }
    
    /**
     * Test of getProductById method, of class ProductsRESTFacade.
     */
    @Test
    public void testGetProductById() throws ParseException {
        System.out.println("getProductById");

        int productID = 4;

        given()
                .spec(validator.getRequestSpecification())
                .get("" + productID)
                .then()
                .assertThat()
                .body("id", equalTo(productID));
    }

    /**
     * Test of getProductById method, of class ProductsRESTFacade, but with JSONSchema.
     */
    @Test
    public void testGetProductByIdJSON() {
        System.out.println("getProductById with JSON schema");

        int productID = 4;

        validator.getRequest("" + productID, "Product.json");
    }

    /**
     * Test of getAllProducts method, of class ProductsRESTFacade.
     */
    @Test
    public void testgetProductsStockJSON() {
        System.out.println("getAllProducts with JSON schema");

        validator.getRequest("all", "All_Products.json");
    }

    /**
     * Test of updateStock method, of class ProductsRESTFacade.
     */
    @Test
    public void testUpdateStock() {
        System.out.println("updateStock");
        
        JSONObject product = new JSONObject();
        product.put("amount", 50);
        product.put("productID", 14);

        Response res = validator.postRequest("update", product);

        assertTrue(res.asString().contains("updated"));
    }


    /**
     * Test of updateProduct method, of class ProductsRESTFacade.
     */
    @Test
    public void testUpdateProduct() {
        System.out.println("updateProduct");
        
        JSONObject product = new JSONObject();
        product.put("productID", 14);
        product.put("buyprice", 0.95);
        product.put("desc", "Integration test is working");
        product.put("name", "Fristi");
        
        Response res = validator.postRequest("editProduct", product);
        
        assertTrue(res.asString().contains("edited"));
    }
}
