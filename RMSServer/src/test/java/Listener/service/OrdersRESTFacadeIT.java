/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.response.Response;
import static org.hamcrest.Matchers.equalTo;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class OrdersRESTFacadeIT {

    private RestValidator validator, checkout;

    public OrdersRESTFacadeIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        validator = new RestValidator("orders");
        checkout = new RestValidator("tables");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAllOpenOrders method, of class OrdersRESTFacade.
     */
    @Test
    public void testGetAllOpenOrders() {
        System.out.println("getAllOpenOrders");

        validator.getRequest("pending", "Orders_Pending.json");
    }

    /**
     * Tests the entire order system by creating a new order, adding items to
     * it, and deleting them again
     */
    @Test
    public void testOrders() {
        System.out.println("create");

        int tableID = 74;
        int employeeID = 34;
        int[] menuItemsArray = {4, 94, 94, 95};

        // Create new orderJSONObject with menuItems
        JSONArray menuItems = new JSONArray();
        for (int i = 0; i < menuItemsArray.length; i++) {
            menuItems.add(menuItemsArray[i]);
        }

        JSONObject newOrder = new JSONObject();
        newOrder.put("tableid", tableID);
        newOrder.put("employeeid", employeeID);
        newOrder.put("menuitems", menuItems);

        // Add the new order
        Response resOrder = validator.postRequest("add", newOrder);

        // Check if order is created
        assertTrue(resOrder.asString().contains("created"));

        // Check if order is conform with JSON Schema
        validator.getRequest("" + tableID, "Order.json");

        // Retreive the orderId 
        Response OrderResponse = given()
                .spec(validator.getRequestSpecification())
                .get("" + tableID);

        int orderID = OrderResponse.path("orderId");

        // Remove all menuItems that were previously added
        for (int i = 0; i < menuItemsArray.length; i++) {
            // Create menuItems to cancel
            JSONObject cancelMenuItem = new JSONObject();
            cancelMenuItem.put("orderId", orderID);
            cancelMenuItem.put("menuItemId", menuItemsArray[i]);

            // Remove item from order
            Response resCancel = validator.postRequest("cancel/menuItem", cancelMenuItem);

            // Check item is cancelled
            assertTrue(resCancel.asString().contains("remove"));

        }
        System.out.println("/checkout/" + tableID);

        Response res = given()
                .spec(checkout.getRequestSpecification())
                .get("checkout/" + tableID);
        assertTrue(res.asString().contains("checkedout"));
    }

}
