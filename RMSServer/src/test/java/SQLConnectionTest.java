/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Handlers.*;
import SQLConnector.SQLConnection;
import static DataLayer.rmsdata.tables.Products.PRODUCTS;
import DataLayer.rmsdata.tables.records.ProductsRecord;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.jooq.Record;
import org.jooq.Result;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class SQLConnectionTest {

    public SQLConnectionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
//        String port = System.getProperty("server.port");
//        if (port == null) {
//            RestAssured.port = Integer.valueOf(8084);
//        } else {
//            RestAssured.port = Integer.valueOf(port);
//        }
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of makeConnection method, of class TableHandler.
     */
    @Test
    public void testMakeConnection() throws SQLException {
        System.out.println("makeConnection");
        TableHandler instance = new TableHandler();
        boolean expResult = true;
        boolean result = instance.makeConnection();
        assertEquals(expResult, result);
    }

    /**
     * Test of Close method, of class TableHandler.
     */
    @Test
    public void testClose() throws Exception {
        System.out.println("Close");
        TableHandler instance = new TableHandler();
        instance.Close();
    }

    /**
     * Tests if correct product is returned from database
     */
    @Test
    public void testOneProduct() throws SQLException {
        System.out.println("Test product");
        ProductHandler handler = new ProductHandler();
        ProductsRecord product = handler.TestProduct(14);

        assertEquals(14, (int) product.getId());
    }

    /**
     * Tests if correct products are returned from database + print in console
     */
    @Test
    public void testMultipleProducts() throws SQLException {
        System.out.println("Test products");
        ProductHandler handler = new ProductHandler();
        Result<ProductsRecord> products = handler.TestProducts();
        List<ProductsRecord> list = new ArrayList<ProductsRecord>();

        for (Record product : products) {
            list.add((ProductsRecord) product);
            int productId = product.getValue(PRODUCTS.ID);
            String productName = product.getValue(PRODUCTS.NAME);
            double productPrice = product.getValue(PRODUCTS.BUYPRICE);

            System.out.println("Product id: " + productId + ", product name: " + productName + ", price: " + productPrice);
        }

        assertEquals(4, (int) (list.get(0).getId()), 0);
    }
}
