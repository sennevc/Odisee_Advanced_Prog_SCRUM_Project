var RMSApi = new function () {
    this.ajaxGet = function(url, callback) {
        callback = callback || function() {};
        
        $.ajax({ 
            url: url, 
            type: 'get', 
            dataType: 'json',
            success: function (data, textStatus, jqXHR) { 
                callback(data); 
            }, 
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown); 
            }, 
            statusCode: { 
                404: function () { 
                    console.log('404'); 
                } 
            } 
        });
    };
    
    this.ajaxPost = function(url, data) {
        $.ajax({ 
            url: url, 
            headers: { 
                'Accept': 'text/plain', 
                'Content-Type': 'text/plain' 
            }, 
            type: 'post', 
            dataType: 'text', 
            data: JSON.stringify(data), 
            success: function (data, textStatus, jqXHR) { 
            }, 
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown); 
            }, 
            statusCode: { 
                404: function () { 
                    console.log('404'); 
                } 
            } 
        });
    };
};