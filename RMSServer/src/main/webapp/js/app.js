/*
    app-wide javascript for general event binding
 */
var RMSApp = new function() {

    /**
     * Toggles a view by classname
     * @param viewName CSS-class view-name to change to
     */
    this.toggleView = function (viewName) {
        // Does this view enable order-panel?
        RMSApp.enableOrderSidePanel = viewName == '.view-order-for-table';
        
        $('.view').addClass('hidden');
        $(viewName).toggleClass('hidden');
        
        if(viewName === '.view-order-for-table') {
            $('#showOrderPanel').removeClass('hidden');
        } else {
            $('#showOrderPanel').addClass('hidden');
            RMSTables.clearOrder();
        }
        
    };

    this.ajax = function(apiURL, dataType, httpType, success, data) {
        var headers = null;

        if (success === null) success = function() {};
        if (httpType === 'post'){
            headers = {
                'Accept': 'text/plain',
                'Content-Type': 'text/plain'
            };
        }
        if (data === null) {
            data = {};
        }
    };

    // Allow/disallow users to acces the order-side-panel on the right
    this.enableOrderSidePanel = false;

    // Keep track of pages for back-key functions
    this.history = [];

    this.resetHistory = function() {
        RMSApp.history = [{
            title: 'Tables',
            view: '.view-tables-all',
            action: null
        }];

        // Push view into history
        RMSApp.history.push({
            title: 'Orders',
            view: '.view-orders-all',
            action: RMSOrders.loadOrders
        });
    };

    this.swipe = {
        POSITION_LEFT: 0,
        POSITION_MIDDLE: 1,
        POSITION_RIGHT: 2,
        SWIPE_LEFT: 1,
        SWIPE_RIGHT: -1,

        currentPos: 1,

        commitSwipe: function() {
            if (RMSApp.swipe.currentPos == RMSApp.swipe.POSITION_LEFT) {
                $('#menuPanel').addClass('side-panel-shown-left');
                $('.cover-menu').fadeIn(200);
            }

            if (RMSApp.swipe.currentPos == RMSApp.swipe.POSITION_MIDDLE) {
                $('#menuPanel').removeClass('side-panel-shown-left');
                $('#orderPanel').removeClass('side-panel-shown-right');
                $('.cover-menu').fadeOut(200);
                $('.cover-side').fadeOut(200);
            }

            if (RMSApp.swipe.currentPos == RMSApp.swipe.POSITION_RIGHT) {
                if (RMSApp.enableOrderSidePanel) {
                    $('#orderPanel').addClass('side-panel-shown-right');
                    $('.cover-side').fadeIn(200);
                }
                else {
                    RMSApp.swipe.currentPos = RMSApp.swipe.POSITION_MIDDLE;
                }
            }
        },

        left: function() {
            if (RMSApp.swipe.currentPos != RMSApp.swipe.POSITION_RIGHT) {
                // Shift the view to the left
                RMSApp.swipe.currentPos += RMSApp.swipe.SWIPE_LEFT;
            }

            RMSApp.swipe.commitSwipe();
        },

        right: function() {
            if (RMSApp.swipe.currentPos != RMSApp.swipe.POSITION_LEFT) {
                // Shift the view to the left
                RMSApp.swipe.currentPos += RMSApp.swipe.SWIPE_RIGHT;
            }

            RMSApp.swipe.commitSwipe();
        }
    };

    // toggles the menu panel and the cover (to darken the background)
    this.toggleMenuPanel = function () {
        $('#menuPanel').toggleClass('side-panel-shown-left');
        $('.cover-menu').fadeToggle(200);

        // close other panel
        if ($('#menuPanel').hasClass('side-panel-shown-left') && $('#orderPanel').hasClass('side-panel-shown-right')) {
            RMSApp.toggleOrderPanel();
        }

        // Swipe synchronisation
        if ($('#menuPanel').hasClass('side-panel-shown-left')) {
            RMSApp.swipe.currentPos = RMSApp.swipe.POSITION_LEFT;
        }
        else {
            RMSApp.swipe.currentPos = RMSApp.swipe.POSITION_MIDDLE;
        }
    };

    // toggles the order panel and the cover (to darken the background)
    this.toggleOrderPanel = function () {
        $('#orderPanel').toggleClass('side-panel-shown-right');
        $('.cover-side').fadeToggle(200);

        // close other panel
        if ($('#menuPanel').hasClass('side-panel-shown-left') && $('#orderPanel').hasClass('side-panel-shown-right')) {
            RMSApp.toggleMenuPanel();
        }

        // Swipe synchronisation
        if ($('#orderPanel').hasClass('side-panel-shown-right')) {
            RMSApp.swipe.currentPos = RMSApp.swipe.POSITION_RIGHT;
        }
        else {
            RMSApp.swipe.currentPos = RMSApp.swipe.POSITION_MIDDLE;
        }
    };
};

$(document).ready(function () {

    // Initialize SWIPES
    var swipeListener = new Hammer(document.getElementsByTagName('html')[0]);

    swipeListener.on('swipe', function(ev) {
        if (ev.direction == Hammer.DIRECTION_RIGHT) RMSApp.swipe.right();
        if (ev.direction == Hammer.DIRECTION_LEFT) RMSApp.swipe.left();
    });

    // Stick home view in history
    RMSApp.history = [{
        title: 'Tables',
        view: '.view-tables-all',
        action: null
    }];

    // Back Key
    $('#btnBack').on('click', function() {
        if (RMSApp.history.length > 1) {
            // Erase current view since we are leaving
            RMSApp.history.pop();

            // Now find the last view
            var previous = RMSApp.history[RMSApp.history.length - 1];

            // Does this view require to load stuff?
            if (previous.action !== null) {
                previous.action();
            }

            $('.header-title').text(previous.title);

            RMSApp.toggleView(previous.view);
        }
    });

    // Initialize home view
    RMSTables.init();

    // Menu-button to show the general order-view
    $('#btnOrdersMenu').on('click', function () {
        RMSOrders.loadOrders();
        $('.header-title').text('Orders');
        RMSApp.toggleView('.view-orders-all');
        RMSApp.toggleMenuPanel();

        RMSApp.resetHistory();
    });

    // Menu-button to display the table overview
    $('#btnTablesMenu').on('click', function () {
        RMSApp.toggleView('.view-tables-all');
        RMSApp.toggleMenuPanel();
        $('.header-title').text('Tables');

        RMSApp.resetHistory();
    });

    // Make menu panel appear
    $('#showMenuPanel').on('click', RMSApp.toggleMenuPanel);

    // Make drawer panel appear
    $('#showOrderPanel').on('click', RMSApp.toggleOrderPanel);

    // Tapping next to menu panel closes it:
    $('.cover-menu').on('click', RMSApp.toggleMenuPanel);

    // Tapping next to side panel closes it:
    $('.cover-side').on('click', RMSApp.toggleOrderPanel);
});