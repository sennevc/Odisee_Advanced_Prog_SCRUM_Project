/**
 * This class is generated by jOOQ
 */
package DataLayer.rmsdata.tables;


import DataLayer.rmsdata.Keys;
import DataLayer.rmsdata.Rmsdata;
import DataLayer.rmsdata.tables.records.TablesRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables extends TableImpl<TablesRecord> {

    private static final long serialVersionUID = -483261684;

    /**
     * The reference instance of <code>RMSData.tables</code>
     */
    public static final Tables TABLES = new Tables();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TablesRecord> getRecordType() {
        return TablesRecord.class;
    }

    /**
     * The column <code>RMSData.tables.id</code>.
     */
    public final TableField<TablesRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>RMSData.tables.max_seats</code>.
     */
    public final TableField<TablesRecord, Integer> MAX_SEATS = createField("max_seats", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>RMSData.tables.position</code>.
     */
    public final TableField<TablesRecord, String> POSITION = createField("position", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * Create a <code>RMSData.tables</code> table reference
     */
    public Tables() {
        this("tables", null);
    }

    /**
     * Create an aliased <code>RMSData.tables</code> table reference
     */
    public Tables(String alias) {
        this(alias, TABLES);
    }

    private Tables(String alias, Table<TablesRecord> aliased) {
        this(alias, aliased, null);
    }

    private Tables(String alias, Table<TablesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Rmsdata.RMSDATA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<TablesRecord, Integer> getIdentity() {
        return Keys.IDENTITY_TABLES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TablesRecord> getPrimaryKey() {
        return Keys.KEY_TABLES_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TablesRecord>> getKeys() {
        return Arrays.<UniqueKey<TablesRecord>>asList(Keys.KEY_TABLES_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Tables as(String alias) {
        return new Tables(alias, this);
    }

    /**
     * Rename this table
     */
    public Tables rename(String name) {
        return new Tables(name, null);
    }
}
