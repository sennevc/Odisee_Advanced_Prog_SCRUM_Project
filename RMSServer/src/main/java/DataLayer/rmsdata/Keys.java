/**
 * This class is generated by jOOQ
 */
package DataLayer.rmsdata;


import DataLayer.rmsdata.tables.Categories;
import DataLayer.rmsdata.tables.Employees;
import DataLayer.rmsdata.tables.Menuitems;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.Orders;
import DataLayer.rmsdata.tables.OrdersHasMenuitems;
import DataLayer.rmsdata.tables.Products;
import DataLayer.rmsdata.tables.Stock;
import DataLayer.rmsdata.tables.Tables;
import DataLayer.rmsdata.tables.records.CategoriesRecord;
import DataLayer.rmsdata.tables.records.EmployeesRecord;
import DataLayer.rmsdata.tables.records.MenuitemsHasProductsRecord;
import DataLayer.rmsdata.tables.records.MenuitemsRecord;
import DataLayer.rmsdata.tables.records.OrdersHasMenuitemsRecord;
import DataLayer.rmsdata.tables.records.OrdersRecord;
import DataLayer.rmsdata.tables.records.ProductsRecord;
import DataLayer.rmsdata.tables.records.StockRecord;
import DataLayer.rmsdata.tables.records.TablesRecord;

import javax.annotation.Generated;

import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.UniqueKey;
import org.jooq.impl.AbstractKeys;


/**
 * A class modelling foreign key relationships between tables of the <code>RMSData</code> 
 * schema
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<CategoriesRecord, Integer> IDENTITY_CATEGORIES = Identities0.IDENTITY_CATEGORIES;
    public static final Identity<EmployeesRecord, Integer> IDENTITY_EMPLOYEES = Identities0.IDENTITY_EMPLOYEES;
    public static final Identity<MenuitemsRecord, Integer> IDENTITY_MENUITEMS = Identities0.IDENTITY_MENUITEMS;
    public static final Identity<OrdersRecord, Integer> IDENTITY_ORDERS = Identities0.IDENTITY_ORDERS;
    public static final Identity<OrdersHasMenuitemsRecord, Integer> IDENTITY_ORDERS_HAS_MENUITEMS = Identities0.IDENTITY_ORDERS_HAS_MENUITEMS;
    public static final Identity<ProductsRecord, Integer> IDENTITY_PRODUCTS = Identities0.IDENTITY_PRODUCTS;
    public static final Identity<TablesRecord, Integer> IDENTITY_TABLES = Identities0.IDENTITY_TABLES;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<CategoriesRecord> KEY_CATEGORIES_PRIMARY = UniqueKeys0.KEY_CATEGORIES_PRIMARY;
    public static final UniqueKey<EmployeesRecord> KEY_EMPLOYEES_PRIMARY = UniqueKeys0.KEY_EMPLOYEES_PRIMARY;
    public static final UniqueKey<EmployeesRecord> KEY_EMPLOYEES_USERNAME_UNIQUE = UniqueKeys0.KEY_EMPLOYEES_USERNAME_UNIQUE;
    public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_PRIMARY = UniqueKeys0.KEY_MENUITEMS_PRIMARY;
    public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_ID_UNIQUE = UniqueKeys0.KEY_MENUITEMS_ID_UNIQUE;
    public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_NAME_UNIQUE = UniqueKeys0.KEY_MENUITEMS_NAME_UNIQUE;
    public static final UniqueKey<MenuitemsHasProductsRecord> KEY_MENUITEMS_HAS_PRODUCTS_PRIMARY = UniqueKeys0.KEY_MENUITEMS_HAS_PRODUCTS_PRIMARY;
    public static final UniqueKey<OrdersRecord> KEY_ORDERS_PRIMARY = UniqueKeys0.KEY_ORDERS_PRIMARY;
    public static final UniqueKey<OrdersHasMenuitemsRecord> KEY_ORDERS_HAS_MENUITEMS_PRIMARY = UniqueKeys0.KEY_ORDERS_HAS_MENUITEMS_PRIMARY;
    public static final UniqueKey<ProductsRecord> KEY_PRODUCTS_PRIMARY = UniqueKeys0.KEY_PRODUCTS_PRIMARY;
    public static final UniqueKey<StockRecord> KEY_STOCK_PRIMARY = UniqueKeys0.KEY_STOCK_PRIMARY;
    public static final UniqueKey<TablesRecord> KEY_TABLES_PRIMARY = UniqueKeys0.KEY_TABLES_PRIMARY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<MenuitemsRecord, CategoriesRecord> FK_MENUITEMS_CATEGORIES1 = ForeignKeys0.FK_MENUITEMS_CATEGORIES1;
    public static final ForeignKey<MenuitemsHasProductsRecord, MenuitemsRecord> FK_MENUITEMS_HAS_PRODUCTS_MENUITEMS1 = ForeignKeys0.FK_MENUITEMS_HAS_PRODUCTS_MENUITEMS1;
    public static final ForeignKey<MenuitemsHasProductsRecord, ProductsRecord> FK_MENUITEMS_HAS_PRODUCTS_PRODUCTS1 = ForeignKeys0.FK_MENUITEMS_HAS_PRODUCTS_PRODUCTS1;
    public static final ForeignKey<OrdersRecord, TablesRecord> FK_ORDERS_TABLES1 = ForeignKeys0.FK_ORDERS_TABLES1;
    public static final ForeignKey<OrdersRecord, EmployeesRecord> FK_ORDERS_EMPLOYEES = ForeignKeys0.FK_ORDERS_EMPLOYEES;
    public static final ForeignKey<OrdersHasMenuitemsRecord, OrdersRecord> FK_ORDERS_HAS_MENUITEMS_ORDERS1 = ForeignKeys0.FK_ORDERS_HAS_MENUITEMS_ORDERS1;
    public static final ForeignKey<OrdersHasMenuitemsRecord, MenuitemsRecord> FK_ORDERS_HAS_MENUITEMS_MENUITEMS1 = ForeignKeys0.FK_ORDERS_HAS_MENUITEMS_MENUITEMS1;
    public static final ForeignKey<StockRecord, ProductsRecord> FK_STOCK_PRODUCTS1 = ForeignKeys0.FK_STOCK_PRODUCTS1;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 extends AbstractKeys {
        public static Identity<CategoriesRecord, Integer> IDENTITY_CATEGORIES = createIdentity(Categories.CATEGORIES, Categories.CATEGORIES.ID);
        public static Identity<EmployeesRecord, Integer> IDENTITY_EMPLOYEES = createIdentity(Employees.EMPLOYEES, Employees.EMPLOYEES.ID);
        public static Identity<MenuitemsRecord, Integer> IDENTITY_MENUITEMS = createIdentity(Menuitems.MENUITEMS, Menuitems.MENUITEMS.ID);
        public static Identity<OrdersRecord, Integer> IDENTITY_ORDERS = createIdentity(Orders.ORDERS, Orders.ORDERS.ID);
        public static Identity<OrdersHasMenuitemsRecord, Integer> IDENTITY_ORDERS_HAS_MENUITEMS = createIdentity(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ID);
        public static Identity<ProductsRecord, Integer> IDENTITY_PRODUCTS = createIdentity(Products.PRODUCTS, Products.PRODUCTS.ID);
        public static Identity<TablesRecord, Integer> IDENTITY_TABLES = createIdentity(Tables.TABLES, Tables.TABLES.ID);
    }

    private static class UniqueKeys0 extends AbstractKeys {
        public static final UniqueKey<CategoriesRecord> KEY_CATEGORIES_PRIMARY = createUniqueKey(Categories.CATEGORIES, "KEY_categories_PRIMARY", Categories.CATEGORIES.ID);
        public static final UniqueKey<EmployeesRecord> KEY_EMPLOYEES_PRIMARY = createUniqueKey(Employees.EMPLOYEES, "KEY_employees_PRIMARY", Employees.EMPLOYEES.ID);
        public static final UniqueKey<EmployeesRecord> KEY_EMPLOYEES_USERNAME_UNIQUE = createUniqueKey(Employees.EMPLOYEES, "KEY_employees_username_UNIQUE", Employees.EMPLOYEES.USERNAME);
        public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_PRIMARY = createUniqueKey(Menuitems.MENUITEMS, "KEY_menuitems_PRIMARY", Menuitems.MENUITEMS.ID);
        public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_ID_UNIQUE = createUniqueKey(Menuitems.MENUITEMS, "KEY_menuitems_id_UNIQUE", Menuitems.MENUITEMS.ID);
        public static final UniqueKey<MenuitemsRecord> KEY_MENUITEMS_NAME_UNIQUE = createUniqueKey(Menuitems.MENUITEMS, "KEY_menuitems_name_UNIQUE", Menuitems.MENUITEMS.NAME);
        public static final UniqueKey<MenuitemsHasProductsRecord> KEY_MENUITEMS_HAS_PRODUCTS_PRIMARY = createUniqueKey(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS, "KEY_menuitems_has_products_PRIMARY", MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID);
        public static final UniqueKey<OrdersRecord> KEY_ORDERS_PRIMARY = createUniqueKey(Orders.ORDERS, "KEY_orders_PRIMARY", Orders.ORDERS.ID, Orders.ORDERS.TABLE_ID);
        public static final UniqueKey<OrdersHasMenuitemsRecord> KEY_ORDERS_HAS_MENUITEMS_PRIMARY = createUniqueKey(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS, "KEY_orders_has_menuitems_PRIMARY", OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ID);
        public static final UniqueKey<ProductsRecord> KEY_PRODUCTS_PRIMARY = createUniqueKey(Products.PRODUCTS, "KEY_products_PRIMARY", Products.PRODUCTS.ID);
        public static final UniqueKey<StockRecord> KEY_STOCK_PRIMARY = createUniqueKey(Stock.STOCK, "KEY_stock_PRIMARY", Stock.STOCK.PRODUCT_ID);
        public static final UniqueKey<TablesRecord> KEY_TABLES_PRIMARY = createUniqueKey(Tables.TABLES, "KEY_tables_PRIMARY", Tables.TABLES.ID);
    }

    private static class ForeignKeys0 extends AbstractKeys {
        public static final ForeignKey<MenuitemsRecord, CategoriesRecord> FK_MENUITEMS_CATEGORIES1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_CATEGORIES_PRIMARY, Menuitems.MENUITEMS, "fk_menuitems_categories1", Menuitems.MENUITEMS.CATEGORIES_ID);
        public static final ForeignKey<MenuitemsHasProductsRecord, MenuitemsRecord> FK_MENUITEMS_HAS_PRODUCTS_MENUITEMS1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_MENUITEMS_PRIMARY, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS, "fk_menuitems_has_products_menuitems1", MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID);
        public static final ForeignKey<MenuitemsHasProductsRecord, ProductsRecord> FK_MENUITEMS_HAS_PRODUCTS_PRODUCTS1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_PRODUCTS_PRIMARY, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS, "fk_menuitems_has_products_products1", MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID);
        public static final ForeignKey<OrdersRecord, TablesRecord> FK_ORDERS_TABLES1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_TABLES_PRIMARY, Orders.ORDERS, "fk_orders_tables1", Orders.ORDERS.TABLE_ID);
        public static final ForeignKey<OrdersRecord, EmployeesRecord> FK_ORDERS_EMPLOYEES = createForeignKey(DataLayer.rmsdata.Keys.KEY_EMPLOYEES_PRIMARY, Orders.ORDERS, "fk_orders_employees", Orders.ORDERS.EMPLOYEE);
        public static final ForeignKey<OrdersHasMenuitemsRecord, OrdersRecord> FK_ORDERS_HAS_MENUITEMS_ORDERS1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_ORDERS_PRIMARY, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS, "fk_orders_has_menuitems_orders1", OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID);
        public static final ForeignKey<OrdersHasMenuitemsRecord, MenuitemsRecord> FK_ORDERS_HAS_MENUITEMS_MENUITEMS1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_MENUITEMS_PRIMARY, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS, "fk_orders_has_menuitems_menuitems1", OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID);
        public static final ForeignKey<StockRecord, ProductsRecord> FK_STOCK_PRODUCTS1 = createForeignKey(DataLayer.rmsdata.Keys.KEY_PRODUCTS_PRIMARY, Stock.STOCK, "fk_stock_products1", Stock.STOCK.PRODUCT_ID);
    }
}
