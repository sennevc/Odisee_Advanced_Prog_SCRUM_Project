/**
 * This class is generated by jOOQ
 */
package DataLayer.rmsdata;


import DataLayer.rmsdata.tables.Categories;
import DataLayer.rmsdata.tables.Employees;
import DataLayer.rmsdata.tables.Menuitems;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.Orders;
import DataLayer.rmsdata.tables.OrdersHasMenuitems;
import DataLayer.rmsdata.tables.Products;
import DataLayer.rmsdata.tables.Stock;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in RMSData
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>RMSData.categories</code>.
     */
    public static final Categories CATEGORIES = DataLayer.rmsdata.tables.Categories.CATEGORIES;

    /**
     * The table <code>RMSData.employees</code>.
     */
    public static final Employees EMPLOYEES = DataLayer.rmsdata.tables.Employees.EMPLOYEES;

    /**
     * The table <code>RMSData.menuitems</code>.
     */
    public static final Menuitems MENUITEMS = DataLayer.rmsdata.tables.Menuitems.MENUITEMS;

    /**
     * The table <code>RMSData.menuitems_has_products</code>.
     */
    public static final MenuitemsHasProducts MENUITEMS_HAS_PRODUCTS = DataLayer.rmsdata.tables.MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS;

    /**
     * The table <code>RMSData.orders</code>.
     */
    public static final Orders ORDERS = DataLayer.rmsdata.tables.Orders.ORDERS;

    /**
     * The table <code>RMSData.orders_has_menuitems</code>.
     */
    public static final OrdersHasMenuitems ORDERS_HAS_MENUITEMS = DataLayer.rmsdata.tables.OrdersHasMenuitems.ORDERS_HAS_MENUITEMS;

    /**
     * The table <code>RMSData.products</code>.
     */
    public static final Products PRODUCTS = DataLayer.rmsdata.tables.Products.PRODUCTS;

    /**
     * The table <code>RMSData.stock</code>.
     */
    public static final Stock STOCK = DataLayer.rmsdata.tables.Stock.STOCK;

    /**
     * The table <code>RMSData.tables</code>.
     */
    public static final DataLayer.rmsdata.tables.Tables TABLES = DataLayer.rmsdata.tables.Tables.TABLES;
}
