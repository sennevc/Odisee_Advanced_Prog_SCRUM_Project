package SQLConnector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import java.sql.*;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author TOM
 */
public class SQLConnection {

    private final String userName;
    private final String password;
    private final String connectionString;
    public DSLContext DBContext;
    protected Connection conn;

    /**
     * Constructor that makes connection with database
     */
    public SQLConnection() throws SQLException {
        userName = "CBAdmin";
        password = "CubaLibre123";
        connectionString = "jdbc:mysql://rmsserver.cokx6ij8cwie.eu-central-1.rds.amazonaws.com";
        conn = DriverManager.getConnection(connectionString, userName, password);
        DBContext = DSL.using(conn, SQLDialect.MYSQL);
    }

    /**
     * Close Connection
     */
    public void Close() throws SQLException {
        if (DBContext != null) {
            DBContext.close();
            conn.close();
        }

    }

    /**
     * Test connection
     *
     * @return boolean if connection succeeded
     */
    public boolean makeConnection() throws SQLException {
        try (Connection connTest = DriverManager.getConnection(connectionString, userName, password)) {
            return true;
        } catch (CommunicationsException e) {
            return false;
        }
    }

}
