/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author basvg
 */
@Embeddable
public class OrdersHasMenuitemsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "orders_id")
    private int ordersId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "menuitems_id")
    private int menuitemsId;

    public OrdersHasMenuitemsPK() {
    }

    public OrdersHasMenuitemsPK(int ordersId, int menuitemsId) {
        this.ordersId = ordersId;
        this.menuitemsId = menuitemsId;
    }

    public int getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(int ordersId) {
        this.ordersId = ordersId;
    }

    public int getMenuitemsId() {
        return menuitemsId;
    }

    public void setMenuitemsId(int menuitemsId) {
        this.menuitemsId = menuitemsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ordersId;
        hash += (int) menuitemsId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersHasMenuitemsPK)) {
            return false;
        }
        OrdersHasMenuitemsPK other = (OrdersHasMenuitemsPK) object;
        if (this.ordersId != other.ordersId) {
            return false;
        }
        if (this.menuitemsId != other.menuitemsId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Listener.OrdersHasMenuitemsPK[ ordersId=" + ordersId + ", menuitemsId=" + menuitemsId + " ]";
    }
    
}
