/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Handlers.OrderHandler;
import javax.persistence.EntityManager;
import Handlers.TableHandler;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("tables")
@Singleton
@Autowire
public class TablesRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    public TablesRESTFacade() {
    }

    /**
     * Returns a JSON Object of all tables
     *
     * @return JSON formatted string (all tables)
     */
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getAllTables() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            TableHandler handler = new TableHandler();
            String tables = handler.getAllTables();
            handler.Close();
            return tables;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Closes the pending order for the table
     *
     * @param id Table id
     * @return Message if successful
     */
    @GET
    @Path("/checkout/{id}")
    @Transactional
    public String checkout(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            handler.checkoutOrder(id);
            handler.Close();
            return "Pending order checkedout";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

}
