/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Listener.OrdersHasMenuitemsPK;
import javax.persistence.EntityManager;
import javax.ws.rs.core.PathSegment;
import Listener.OrdersHasMenuitems;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import java.net.URI;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("listener.ordershasmenuitems")
@Singleton
@Autowire
public class OrdersHasMenuitemsRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    private OrdersHasMenuitemsPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;ordersId=ordersIdValue;menuitemsId=menuitemsIdValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Listener.OrdersHasMenuitemsPK key = new Listener.OrdersHasMenuitemsPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> ordersId = map.get("ordersId");
        if (ordersId != null && !ordersId.isEmpty()) {
            key.setOrdersId(new java.lang.Integer(ordersId.get(0)));
        }
        java.util.List<String> menuitemsId = map.get("menuitemsId");
        if (menuitemsId != null && !menuitemsId.isEmpty()) {
            key.setMenuitemsId(new java.lang.Integer(menuitemsId.get(0)));
        }
        return key;
    }

    public OrdersHasMenuitemsRESTFacade() {
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public Response create(OrdersHasMenuitems entity) {
        entityManager.persist(entity);
        return Response.created(URI.create(entity.getOrdersHasMenuitemsPK().getOrdersId() + "," + entity.getOrdersHasMenuitemsPK().getMenuitemsId())).build();
    }

    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public void edit(OrdersHasMenuitems entity) {
        entityManager.merge(entity);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public void remove(@PathParam("id") PathSegment id) {
        Listener.OrdersHasMenuitemsPK key = getPrimaryKey(id);
        OrdersHasMenuitems entity = entityManager.getReference(OrdersHasMenuitems.class, key);
        entityManager.remove(entity);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public OrdersHasMenuitems find(@PathParam("id") PathSegment id) {
        Listener.OrdersHasMenuitemsPK key = getPrimaryKey(id);
        return entityManager.find(OrdersHasMenuitems.class, key);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public List<OrdersHasMenuitems> findAll() {
        return find(true, -1, -1);
    }

    @GET
    @Path("{max}/{first}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public List<OrdersHasMenuitems> findRange(@PathParam("max") Integer max, @PathParam("first") Integer first) {
        return find(false, max, first);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    @Transactional
    public String count() {
        try {
            Query query = entityManager.createQuery("SELECT count(o) FROM OrdersHasMenuitems AS o");
            return query.getSingleResult().toString();
        } finally {
            entityManager.close();
        }
    }

    private List<OrdersHasMenuitems> find(boolean all, int maxResults, int firstResult) {
        try {
            Query query = entityManager.createQuery("SELECT object(o) FROM OrdersHasMenuitems AS o");
            if (!all) {
                query.setMaxResults(maxResults);
                query.setFirstResult(firstResult);
            }
            return query.getResultList();
        } finally {
            entityManager.close();
        }
    }
    
}
