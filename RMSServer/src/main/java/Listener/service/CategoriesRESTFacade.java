/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Handlers.CategoryHandler;
import javax.persistence.EntityManager;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("categories")
@Singleton
@Autowire
public class CategoriesRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    public CategoriesRESTFacade() {
    }

    /**
     * Returns a JSON Object of one category
     *
     * @return JSON formatted string of the category
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getCategory(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            CategoryHandler con = new CategoryHandler();
            String menuitems = con.getcategorieWithID(id);
            con.Close();
            return menuitems;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of all categories
     *
     * @return JSON formatted string of the categories
     */
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getAllCategories() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            CategoryHandler con = new CategoryHandler();
            String menuitems = con.getAllCategories();
            con.Close();
            return menuitems;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

}
