/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Handlers.MenuItemHandler;
import javax.persistence.EntityManager;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("menu")
@Singleton
@Autowire
public class MenuitemsRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    public MenuitemsRESTFacade() {
    }

    /**
     * Returns a JSON Object of one menu item
     *
     * @return JSON formatted string (menu item)
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getMenuItem(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MenuItemHandler handler = new MenuItemHandler();
            String menuitem = handler.getMenuItem(id);
            handler.Close();
            return menuitem;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of all menu items
     *
     * @return JSON formatted string (menu items)
     */
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getAllMenuItems() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MenuItemHandler handler = new MenuItemHandler();
            String menuitems = handler.getAllMenuItems();
            handler.Close();
            return menuitems;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

//    /**
//     * Edit the price of an item listener
//     *
//     * @param JSONInput Data from the menuItem to edit
//     * @return Message whether price is successfully updated
//     */
//    @POST
//    @Path("edit")
//    @Consumes({MediaType.TEXT_PLAIN})
//    @Transactional
//    public String editPrice(String JSONInput) {
//        try {
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            MenuItemHandler handler = new MenuItemHandler();
//            handler.EditMenuItem(JSONInput);
//            handler.Close();
//            return "price of menuItem successfully edited";
//        } catch (Exception e) {
//            return "{500}" + e;
//        }
//    }

    /**
     * Deletes menuItems from database
     *
     */
    @POST
    @Path("delete/menuItems")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String deleteMenuItems(String orderJSON) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MenuItemHandler handlern = new MenuItemHandler();
            handlern.deleteItems(orderJSON);
            handlern.Close();
            return "MenuItem successfully deleted";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Add menuItem to database
     *
     */
    @POST
    @Path("add/menuItem")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String addMenuItem(String menuJSON) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MenuItemHandler handler = new MenuItemHandler();
            handler.addMenuItem(menuJSON);
            handler.Close();
            return "MenuItem succesfully created";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

//    /**
//     * Add menuItem to database
//     *
//     */
//    @POST
//    @Path("edit/menuItem")
//    @Consumes({MediaType.TEXT_PLAIN})
//    @Transactional
//    public String editMenuItem(String menuJSON) {
//        try {
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            MenuItemHandler handler = new MenuItemHandler();
//            handler.editMenuItem(menuJSON);
//            handler.Close();
//            return "Menuitem successfully edited";
//        } catch (Exception e) {
//            return "{500}" + e;
//        }
//    }
}
