/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Handlers.ProductHandler;
import Handlers.StockHandler;
import javax.persistence.EntityManager;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("products")
@Singleton
@Autowire
public class ProductsRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    public ProductsRESTFacade() {
    }

    /**
     * Returns a JSON Object of a product
     *
     * @return JSON formatted string (product)
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getProductById(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ProductHandler handler = new ProductHandler();
            String product = handler.getProduct(id);
            handler.Close();
            return product;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of all products on orders
     *
     * @return JSON formatted string (all orders)
     */
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getAllProducts() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ProductHandler handler = new ProductHandler();
            String product = handler.getAllProducts();
            handler.Close();
            return product;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of all products and their stock
     *
     * @return JSON formatted string (all products)
     */
    @GET
    @Path("stock")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getProductsStock() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ProductHandler handler = new ProductHandler();
            String products = handler.getAllProducts();
            handler.Close();
            return products;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Update the stock of a product
     *
     * @param JSONInput
     * @return Message if update of the stock was succesful
     */
    @POST
    @Path("update")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String updateStock(String JSONInput) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            StockHandler handler = new StockHandler();
            handler.updateStockProduct(JSONInput);
            handler.Close();
            return "products updated";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Update the properties of a product
     *
     * @param JSONInput Properties of the product
     * @return Message if the edit of the product was succesful
     */
    @POST
    @Path("editProduct")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String updateProduct(String JSONInput) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ProductHandler handler = new ProductHandler();
            handler.updateProduct(JSONInput);
            handler.Close();
            return "product edited";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Deletes products from database
     *
     */
    @POST
    @Path("delete")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String deleteProducts(String productIDs) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ProductHandler handler = new ProductHandler();
            handler.deleteProducts(productIDs);
            handler.Close();
            return "Products successfully deleted";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }
}
