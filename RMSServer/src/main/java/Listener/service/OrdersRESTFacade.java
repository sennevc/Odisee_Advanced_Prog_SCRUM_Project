/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import Handlers.OrderHandler;
import Listener.OrdersPK;
import javax.persistence.EntityManager;
import javax.ws.rs.core.PathSegment;
import com.sun.jersey.api.spring.Autowire;
import com.sun.jersey.spi.resource.Singleton;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author basvg
 */
@Path("orders")
@Singleton
@Autowire
public class OrdersRESTFacade {

    @PersistenceContext(unitName = "com.cubalibre_RMSServer_war_0.1-SNAPSHOTPU")
    protected EntityManager entityManager;

    private OrdersPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;id=idValue;tableId=tableIdValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Listener.OrdersPK key = new Listener.OrdersPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> id = map.get("id");
        if (id != null && !id.isEmpty()) {
            key.setId(new java.lang.Integer(id.get(0)));
        }
        java.util.List<String> tableId = map.get("tableId");
        if (tableId != null && !tableId.isEmpty()) {
            key.setTableId(new java.lang.Integer(tableId.get(0)));
        }
        return key;
    }

    public OrdersRESTFacade() {
    }

    /**
     * Queries the database for all open orders when called
     *
     * @return JSON Object of all open orders
     */
    @GET
    @Path("pending")
    @Produces({MediaType.TEXT_PLAIN})
    @Transactional
    public String getAllOpenOrders() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            String pendingOrders = handler.getAllPendingItems();
            handler.Close();
            return pendingOrders;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * This function gets called when a post is done on the add path. It ads an
     * order to the database
     *
     * @param orderJSON
     * @return Message whether order is successfully created
     */
    @POST
    @Path("add")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String create(String orderJSON) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            handler.createOrder(orderJSON);
            handler.Close();
            return "Order successfully created";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Removes a menu item from an order
     *
     * @param orderJSON JSON Input
     * @return Message whether item is successfully removed from an order
     */
    @POST
    @Path("cancel/menuItem")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String removeMenuItemFromOrder(String orderJSON) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            handler.removeMenuItemFromOrder(orderJSON);
            handler.Close();
            return "Item successfully removed from order";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of all orders
     *
     * @return JSON formatted string (all orders)
     */
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getAllOrders() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            String product = handler.getOrdersAll();
            handler.Close();
            return product;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns a JSON Object of an order
     *
     * @return JSON formatted string (an order)
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getOrderForTable(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            String product = handler.getOpenOrdersForTable(id);
            handler.Close();
            return product;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Returns the total price of an order
     *
     * @param id TableId of the order
     * @return JSON formatted string with price of the total order
     */
    @GET
    @Path("totalOrderPrice/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public String getTotalPriceTable(@PathParam("id") Integer id) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            String price = handler.getTotalOrderPrice(id);
            handler.Close();
            return price;
        } catch (Exception e) {
            return "{500}" + e;
        }
    }

    /**
     * Will set a menuItem from an order to delivered
     *
     * @param dataJSON Json with orderID and the menuItem to be delivered
     * @return Message whether menuItem is successfully delivered
     */
    @POST
    @Path("delivered")
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String delivered(String dataJSON) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            OrderHandler handler = new OrderHandler();
            handler.deliverMenuItem(dataJSON);
            handler.Close();
            return "Menuitem succesfully delivered";
        } catch (Exception e) {
            return "{500}" + e;
        }
    }
}
