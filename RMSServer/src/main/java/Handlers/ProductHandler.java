/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.Tables;
import static DataLayer.rmsdata.Tables.PRODUCTS;
import static DataLayer.rmsdata.Tables.STOCK;
import DataLayer.rmsdata.tables.Products;
import DataLayer.rmsdata.tables.records.ProductsRecord;
import SQLConnector.SQLConnection;
import Tools.JSONTools;
import java.sql.SQLException;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;

/**
 *
 * @author Bas
 */
public class ProductHandler extends SQLConnection {

    /**
     * Constructor that makes connection with database
     */
    public ProductHandler() throws SQLException {
        super();
    }

    /**
     * Retrieves product from database in JSON-format
     *
     * @param id id from the product
     * @return String one product in JSON format
     */
    public String getProduct(int id) {
        ProductsRecord prod = DBContext.selectFrom(Products.PRODUCTS).where(Products.PRODUCTS.ID.equal(id)).fetchOne();
        JSONObject output = new JSONObject();
        output.put("id", prod.getId());
        output.put("name", prod.getName());
        output.put("buyprice", prod.getBuyprice());
        output.put("desc", prod.getDescription());
        return output.toString();
    }

    /**
     * Retrieves multiple products from database
     *
     * @return Result list with all products
     */
    public Result<ProductsRecord> TestProducts() {
        return DBContext.selectFrom(Products.PRODUCTS).fetch();
    }

    /**
     * Retrieves all products with their stock from database
     *
     * @return String all products in JSON format
     */
    public String getAllProducts() {
        Result<?> results = DBContext.select(Products.PRODUCTS.ID.as("id"), Tables.STOCK.QUANTITY.as("quantity"), Products.PRODUCTS.BUYPRICE.as("buyprice"), Products.PRODUCTS.NAME.as("name"))
                .from(Products.PRODUCTS.join(Tables.STOCK).on(Tables.STOCK.PRODUCT_ID.equal(Products.PRODUCTS.ID)))
                .where(PRODUCTS.DESCRIPTION.notEqual("NA"))
                .fetch();
        JSONArray resultset = new JSONArray();
        for (Record item : results) {
            JSONObject output = new JSONObject();
            output.put("id", item.get("id"));
            output.put("name", item.get("name"));
            output.put("buyprice", item.get("buyprice"));
            output.put("quantity", item.get("quantity"));
            resultset.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("products", resultset);
        return result.toString();
    }

    /**
     * Retrieves product from database
     *
     * @param id id from the product
     * @return boolean one product
     */
    public ProductsRecord TestProduct(int id) {
        return DBContext.selectFrom(Products.PRODUCTS).where(Products.PRODUCTS.ID.equal(id)).fetchOne();
    }

    /**
     * Update product or add a new one
     *
     * @param JSONData JSON string containing productID and amount of stock
     *
     */
    public void updateProduct(String JSONData) throws ParseException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        int productID = (int) (long) data.get("productID");
        double buyprice = (double) data.get("buyprice");
        String desc = (String) data.get("desc");
        String name = (String) data.get("name");

        // -1 stands for a new product
        if (productID == -1) {
            ProductsRecord newProduct = DBContext.insertInto(PRODUCTS, PRODUCTS.NAME, PRODUCTS.BUYPRICE, PRODUCTS.DESCRIPTION).values(name, buyprice, desc).returning(PRODUCTS.ID).fetchOne();

            // Put stock at 0 for new item
            DBContext.insertInto(STOCK, STOCK.PRODUCT_ID, STOCK.QUANTITY).values(newProduct.getId(), 0).execute();

        } else {
            // Update existing product
            ProductsRecord product = DBContext.selectFrom(Products.PRODUCTS).where(Products.PRODUCTS.ID.eq(productID)).fetchOne();
            product.setBuyprice(buyprice);
            product.setDescription(desc);
            product.setName(name);
            product.update();
        }
    }

    /**
     * Delete products from database (set description to NA, because foreign key
     * contains it's dangerous to delete a whole product)
     *
     * @param JSONData JSON string containing productIDs
     *
     */
    public void deleteProducts(String JSONData) throws ParseException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        JSONArray productsIDs = (JSONArray) data.get("IDs");
        for (Object productID : productsIDs) {
            Long id = (Long) productID;
            ProductsRecord product = DBContext.selectFrom(PRODUCTS).where(PRODUCTS.ID.equal(Integer.parseInt(id.toString()))).fetchOne();
            product.setDescription("NA");
            product.update();
        }
    }
}
