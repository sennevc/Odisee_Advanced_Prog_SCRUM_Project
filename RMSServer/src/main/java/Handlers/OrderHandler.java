/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.Tables;
import DataLayer.rmsdata.tables.Menuitems;
import static DataLayer.rmsdata.tables.Menuitems.MENUITEMS;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.Orders;
import static DataLayer.rmsdata.tables.Orders.ORDERS;
import DataLayer.rmsdata.tables.OrdersHasMenuitems;
import static DataLayer.rmsdata.tables.OrdersHasMenuitems.ORDERS_HAS_MENUITEMS;
import DataLayer.rmsdata.tables.records.OrdersHasMenuitemsRecord;
import DataLayer.rmsdata.tables.records.OrdersRecord;
import DataLayer.rmsdata.tables.records.StockRecord;
import SQLConnector.SQLConnection;
import Tools.JSONTools;
import static Tools.JSONTools.JSONparser;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;

/**
 *
 * @author Bas
 */
public class OrderHandler extends SQLConnection {

    /**
     * Constructor that makes connection with database
     */
    public OrderHandler() throws SQLException {
        super();
    }

    /**
     * Closes pending order for a table and puts all menuItems to delivered
     *
     * @param tableId ID of the table
     *
     */
    public void checkoutOrder(int tableId) throws ParseException {

        JSONObject openOrder = JSONparser(getOpenOrdersForTable(tableId));
        int orderId = (int) (long) openOrder.get("orderId");
        JSONArray menuItems = (JSONArray) openOrder.get("menuItems");

        if (menuItems.size() > 0) {
            for (Object menuItem : menuItems) {
                JSONObject menuItemJSON = (JSONObject) menuItem;
                int id = (int) (long) menuItemJSON.get("menuId");

                JSONObject deliverItem = new JSONObject();
                deliverItem.put("menuItemID", id);
                deliverItem.put("orderID", orderId);

                deliverMenuItem(deliverItem.toString());
            }
        }

        DBContext.update(Orders.ORDERS).set(Orders.ORDERS.PAID, (byte) 1).where(Orders.ORDERS.TABLE_ID.equal(tableId)).and(Orders.ORDERS.PAID.eq((byte) 0)).execute();
    }

    /**
     * Inserts new order in database
     *
     * @param input data for order
     */
    public void createOrder(String input) throws ParseException {
        JSONObject items = JSONTools.JSONparser(input);
        OrdersRecord order = null;
        OrdersRecord lastOrder;
        int sizeOrders;
        int newOrderId;

        // Get last order from table
        sizeOrders = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.TABLE_ID.equal(Integer.parseInt(items.get("tableid").toString()))).fetch().size();
        if (sizeOrders != 0) {
            order = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.TABLE_ID.equal(Integer.parseInt(items.get("tableid").toString()))).fetch().get(sizeOrders - 1);
        }

        if (order == null) {
            // Create new order
            newOrderId = insertNewOrder(items);
            lastOrder = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.ID.equal(newOrderId)).fetchOne();
        } else if (order.getPaid() == 1) {
            newOrderId = insertNewOrder(items);
            lastOrder = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.ID.equal(newOrderId)).fetchOne();
        } else {
            lastOrder = order;
        }
        createOrderHasMenuItem((JSONArray) items.get("menuitems"), lastOrder.getId());
    }

    /**
     * Retrieves order from database and check whether it's still open
     *
     * @param id id from the table
     * @return String order in JSON format
     */
    public String getOpenOrdersForTable(int id) {
        OrdersRecord order = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.TABLE_ID.equal(id)).and(Orders.ORDERS.PAID.eq((byte) 0)).fetchOne();
        Result<?> orderItems = DBContext.select(Orders.ORDERS.ID.as("orderId"), Orders.ORDERS.TABLE_ID.as("tableId"), Orders.ORDERS.PAID.as("paid"), OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.DELIVERED.as("delivered"), Menuitems.MENUITEMS.ID.as("menuId"), Menuitems.MENUITEMS.SALEPRICE.as("price"), Menuitems.MENUITEMS.NAME.as("menuItemName"), Menuitems.MENUITEMS.DESCRIPTION.as("menuDescription")).from(Orders.ORDERS.join(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS).on(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(Orders.ORDERS.ID))).join(Menuitems.MENUITEMS).on(Menuitems.MENUITEMS.ID.equal(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID)).where(Orders.ORDERS.TABLE_ID.equal(id)).and(Orders.ORDERS.PAID.eq((byte) 0)).orderBy(Menuitems.MENUITEMS.ID.asc()).fetch();
        JSONObject output = new JSONObject();
        if (order != null) {
            output.put("orderId", order.getId());
            output.put("tableId", order.getTableId());
            output.put("employee", order.getEmployee());
            output.put("paid", order.getPaid());
            JSONArray menuItems = new JSONArray();
            for (Record item : orderItems) {
                JSONObject menuItem = new JSONObject();
                menuItem.put("menuId", item.get("menuId"));
                menuItem.put("price", item.get("price"));
                menuItem.put("delivered", item.get("delivered"));
                menuItem.put("menuItemName", item.get("menuItemName"));
                menuItem.put("menuDescription", item.get("menuDescription"));
                menuItems.add(menuItem);
            }
            JSONObject result = new JSONObject();
            output.put("menuItems", menuItems);
        } else {
            // if table has no pending order
            output.put("paid", 1);
        }
        return output.toString();
    }

    /**
     * Retrieves an order with a given id from database
     *
     * @param id order ID
     * @return String all products in JSON format
     */
    public String getOrderWithID(int id) {
        OrdersRecord order = DBContext.selectFrom(Orders.ORDERS).where(Orders.ORDERS.ID.equal(id)).fetchOne();
        JSONObject output = new JSONObject();
        output.put("id", order.getId());
        output.put("tableid", order.getTableId());
        output.put("employee", order.getEmployee());
        output.put("paid", order.getPaid());
        return output.toString();

    }

    /**
     * Inserts menu items for order
     *
     * @param input data for order
     * @param orderID id from the order
     */
    private void createOrderHasMenuItem(JSONArray input, int orderID) {
        List<Integer> menuitems = new ArrayList<Integer>();
        //Link all menitems in the JSON input to the order
        for (Object menuitemid : input) {
            Long id = (Long) menuitemid;
            int currentid = DBContext.insertInto(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID, OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.DELIVERED).values(orderID, Integer.parseInt(id.toString()), (byte) 0).execute();
            menuitems.add(Integer.parseInt(id.toString()));
        }
        updateStock(menuitems);
    }

    /**
     * Retrieves all orders from database
     *
     * @return String all orders in JSON format
     */
    public String getOrdersAll() {
        Result<?> results = DBContext.select(Orders.ORDERS.ID.as("orderId"), Orders.ORDERS.TABLE_ID.as("tableId"), Orders.ORDERS.PAID.as("paid"),
                Orders.ORDERS.EMPLOYEE.as("employeeId"), Menuitems.MENUITEMS.NAME.as("itemName"),
                Menuitems.MENUITEMS.SALEPRICE.as("itemPrice"), Menuitems.MENUITEMS.DESCRIPTION.as("itemDescription"))
                .from(Orders.ORDERS.join(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS)
                        .on(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(Orders.ORDERS.ID)))
                .join(Menuitems.MENUITEMS).on(Menuitems.MENUITEMS.ID.equal(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID))
                .orderBy(Orders.ORDERS.ID.asc()).fetch();
        JSONArray resultset = new JSONArray();
        for (Record record : results) {
            JSONObject output = new JSONObject();
            output.put("orderId", record.get("orderId"));
            output.put("tableId", record.get("tableId"));
            output.put("paid", record.get("paid"));
            output.put("employeeId", record.get("employeeId"));
            output.put("itemName", record.get("itemName"));
            output.put("itemPrice", record.get("itemPrice"));
            output.put("itemDescription", record.get("itemDescription"));
            resultset.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("orders", resultset);
        return result.toString();
    }

    /**
     * Updates stock when items are ordered
     *
     * @param menuitems IDs of the items
     *
     */
    private void updateStock(List<Integer> menuitems) {
        Result<?> products;
        for (int menuitem : menuitems) {
            products = DBContext.select(Tables.STOCK.PRODUCT_ID.as("product_id")).from(Menuitems.MENUITEMS.join(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS).on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(Menuitems.MENUITEMS.ID))).join(Tables.STOCK).on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.equal(Tables.STOCK.PRODUCT_ID)).where(Menuitems.MENUITEMS.ID.equal(menuitem)).fetch();
            for (Record product : products) {
                StockRecord stock = DBContext.selectFrom(Tables.STOCK).where(Tables.STOCK.PRODUCT_ID.equal(Integer.parseInt(product.get("product_id").toString()))).fetchOne();
                if (Integer.parseInt(stock.getQuantity().toString()) != 0) {
                    stock.setQuantity(stock.getQuantity() - 1);
                    stock.update();
                }
            }
        }
    }

    /**
     * Inserts a new order
     *
     * @param item JSON object containing item
     * @return the id of the inserted item
     */
    private int insertNewOrder(JSONObject item) {
        return DBContext.insertInto(Orders.ORDERS, Orders.ORDERS.TABLE_ID, Orders.ORDERS.EMPLOYEE, Orders.ORDERS.PAID).values(Integer.parseInt(item.get("tableid").toString()), Integer.parseInt(item.get("employeeid").toString()), (byte) 0).returning(Orders.ORDERS.ID).fetchOne().getId();
    }

    /**
     * Remove menu item from order
     *
     * @param JSONData JSON string containing orderID and menuID
     *
     */
    public void removeMenuItemFromOrder(String JSONData) throws ParseException, SQLException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        int menuId = (int) (long) data.get("menuItemId");
        int orderId = (int) (long) data.get("orderId");
        StockHandler stock = new StockHandler();
        stock.makeConnection();
        stock.replenishStockUponCancelling(menuId);
        stock.Close();
        OrdersHasMenuitemsRecord item = DBContext.selectFrom(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS).where(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(orderId)).and(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID.equal(menuId)).limit(1).fetchOne();
        item.delete();
    }

    /**
     * Returns all items which have not been delivered
     *
     * @return JSON String of pending items
     * @throws ParseException
     */
    public String getAllPendingItems() throws ParseException {
        Result<?> results = DBContext.select(Orders.ORDERS.ID.as("orderId"), Orders.ORDERS.TABLE_ID.as("tableId"),
                Orders.ORDERS.PAID.as("paid"), Orders.ORDERS.EMPLOYEE.as("employeeId"), Menuitems.MENUITEMS.NAME.as("itemName"), Menuitems.MENUITEMS.ID.as("itemID"),
                Menuitems.MENUITEMS.SALEPRICE.as("itemPrice"), Menuitems.MENUITEMS.DESCRIPTION.as("itemDescription"))
                .from(Orders.ORDERS.join(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS)
                        .on(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(Orders.ORDERS.ID)))
                .join(Menuitems.MENUITEMS).on(Menuitems.MENUITEMS.ID.equal(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID))
                .where(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.DELIVERED.equal((byte) 0))
                .orderBy(Orders.ORDERS.ID.asc())
                .fetch();

        JSONArray resultset = new JSONArray();
        for (Record record : results) {
            JSONObject output = new JSONObject();
            output.put("orderId", record.get("orderId"));
            output.put("tableId", record.get("tableId"));
            output.put("paid", record.get("paid"));
            output.put("employeeId", record.get("employeeId"));
            output.put("itemName", record.get("itemName"));
            output.put("itemPrice", record.get("itemPrice"));
            output.put("itemDescription", record.get("itemDescription"));
            output.put("itemID", record.get("itemID"));
            resultset.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("orders", resultset);
        return result.toString();
    }

    /**
     * Get total price of an order
     *
     * @param tableId  The id of the 
     * @return JSON Data
     *
     */
    public String getTotalOrderPrice(int tableId) throws ParseException {
        double totalPrice = 0;

        Result<?> orderMenuItems = DBContext.select(ORDERS.ID.as("orderId"), MENUITEMS.ID.as("menuId"), MENUITEMS.SALEPRICE.as("price"),
                MENUITEMS.NAME.as("menuItemName"))
                .from(ORDERS.join(ORDERS_HAS_MENUITEMS)
                        .on(ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(ORDERS.ID)))
                .join(MENUITEMS)
                .on(MENUITEMS.ID.equal(ORDERS_HAS_MENUITEMS.MENUITEMS_ID))
                .where(ORDERS.TABLE_ID.equal(tableId)).and(ORDERS.PAID.eq((byte) 0))
                .orderBy(MENUITEMS.ID.asc())
                .fetch();

        for (Record menuItem : orderMenuItems) {
            totalPrice += (double) menuItem.get("price");
        }

        JSONObject result = new JSONObject();
        result.put("totalPrice", totalPrice);
        return result.toString();
    }

    /**
     * Sets a menuitem from an order to delivered
     *
     * @param dataJSON JSON with menuItemID and orderID
     * @throws ParseException
     */
    public void deliverMenuItem(String dataJSON) throws ParseException {
        JSONObject data = JSONTools.JSONparser(dataJSON);
        int menuItemId = (int) (long) data.get("menuItemID");
        int orderId = (int) (long) data.get("orderID");

        OrdersHasMenuitemsRecord item = DBContext.selectFrom(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS).where(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(orderId)).and(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID.equal(menuItemId)).and(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.DELIVERED.eq((byte) 0)).limit(1).fetchOne();
        if (item != null) {
            item.setDelivered((byte) 1);
            item.update();
        }

    }
}
