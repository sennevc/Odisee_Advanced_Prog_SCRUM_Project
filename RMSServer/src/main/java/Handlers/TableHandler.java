/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.tables.Tables;
import DataLayer.rmsdata.tables.records.TablesRecord;
import SQLConnector.SQLConnection;
import java.sql.SQLException;
import org.jooq.Result;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;

/**
 *
 * @author Bas
 */
public class TableHandler extends SQLConnection {


    /**
     * Constructor that makes connection with database
     * @throws java.sql.SQLException
     */
    public TableHandler() throws SQLException {
        super();
    }
    

    /**
     * Retrieves all tables from database
     *
     * @return JSON data in string
     */
    public String getAllTables() {
        Result<TablesRecord> tableresults = DBContext.selectFrom(Tables.TABLES).fetch();
        //Put retrieved tables in a JSON Array
        JSONArray tables = new JSONArray();
        for (TablesRecord table : tableresults) {
            JSONObject output = new JSONObject();
            output.put("tableId", table.getId());
            output.put("seats", table.getMaxSeats());
            tables.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("tables", tables);
        return result.toString();
    }
}
