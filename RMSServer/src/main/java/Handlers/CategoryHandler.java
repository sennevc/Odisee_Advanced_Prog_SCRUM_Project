/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.tables.Categories;
import DataLayer.rmsdata.tables.records.CategoriesRecord;
import SQLConnector.SQLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;

/**
 *
 * @author Bas
 */
public class CategoryHandler extends SQLConnection {


    /**
     * Constructor that makes connection with database
     */
    public CategoryHandler() throws SQLException {
        super();
    }

    /**
     * Retrieves a category with a given id from database
     *
     * @param id cat ID
     * @return String all products in JSON format
     */
    public String getcategorieWithID(int id) {
        CategoriesRecord order = DBContext.selectFrom(Categories.CATEGORIES).where(Categories.CATEGORIES.ID.equal(id)).fetchOne();
        JSONObject output = new JSONObject();
        output.put("id", order.getId());
        output.put("name", order.getName());
        return output.toString();
    }

    /**
     * Retrieves all categories items from database
     *
     * @return String all categories in JSON format
     */
    public String getAllCategories() {
        Result<CategoriesRecord> results = DBContext.selectFrom(Categories.CATEGORIES).fetch();
        JSONArray resultset = new JSONArray();
        for (CategoriesRecord item : results) {
            JSONObject output = new JSONObject();
            output.put("id", item.getId());
            output.put("name", item.getName());
            resultset.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("categories", resultset);
        return result.toString();
    }
}
